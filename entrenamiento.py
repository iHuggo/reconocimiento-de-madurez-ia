import neurolab as nl
import pylab as pl
import numpy as np

np.set_printoptions(suppress=True)

entrada = [
[0.43, 0.29, 29.35, 0.0, 8.01, 28.34, 9.72, 14.61],
[0.13, 0.0, 0.18, 0.0, 0.0, 54.84, 40.33, 3.82],
[0.89, 0.0, 6.54, 0.0, 0.0, 60.14, 32.43, 0.0],
[8.06, 0.0, 2.32, 0.0, 0.0, 27.27, 37.28, 13.92],
[1.19, 0.0, 7.78, 0.0, 0.0, 39.35, 26.31, 15.87],
[0.68, 0.0, 2.39, 0.0, 0.0, 50.53, 32.58, 13.83],
[1.28, 0.0, 0.21, 0.0, 0.0, 73.72, 23.38, 1.41]
]

target = [
[0,1,0,0,0],
[0,0,1,0,0],
[0,0,1,0,0],
[0,0,1,0,0],
[0,0,1,0,0],
[0,0,1,0,0],
[0,0,1,0,0]
]

rangoentradas = [[0,100],[0,100],[0,100],[0,100],[0,100],[0,100],[0,100],[0,100]]

net = nl.net.newff(rangoentradas, [40, 5])

print("Entradas:",net.ci)
print("Salidas:",net.co)
# ver tipo de train
#print(net.trainf)
#net.trainf = nl.train.train_gd

error = net.train(entrada, target, epochs=500000, show=10, goal=0.01)

var = [entrada[0]]
#print (var)
out = net.sim(var)

print ("\n\nSalida\n",out)
print ("\n\nError final:")
print (error[-1])

"""
print ("\n\nPesos:\n")
print (len(net.layers))
for i in range(0,len(net.layers)):
    print ("Net layer", i)
    print (net.layers[i].np['w'])"""
