#!/usr/bin/env python
# -*- coding: utf-8 -*-
from PIL import Image
from PIL import Image as im
from PIL import ImageTk
import pylab as pl
import neurolab as nl
import webcolors
import cv2
import numpy as np
import sys


#al ejecutar el programa en consola se le envia el nombre de la imagen nameimage guarda este nombre
nameimage = sys.argv[1]


#cada una de estas variables es una gama de calores que contiene la imagen a evaluar
coloresrojos = ('thistle','rosybrown','lightsalmon','salmon','darksalmon','lightcoral','indianred','crimson','firebrick','darkred','red','maroon','palevioletred','peachpuff','brown')
coloresnaranjas = ('blanchedalmond','orangered','tomato','coral','darkorange','orange','sandybrown')
coloresverdeoscuro = ('olivedrab','darkolivegreen','limegreen','darkseagreen','mediumaquamarine','mediumseagreen','seagreen','forestgreen','green','darkgreen','olive')
coloresverdeclaro = ('lime','palegoldenrod','lawngreen','chartreuse','greenyellow','springgreen','mediumspringgreen','lightgreen','palegreen','yellowgreen','darkkhaki')
coloresamarillosclaro= ('lightyellow','lemonchiffon','lightgoldenrodyellow','papayawhip','moccasin','khaki')
coloresamarillososcuro = ('yellow','lightyellow','goldenrod','darkgoldenrod')
colorescafe = ('peru','chocolate','saddlebrown','sienna','tan')
coloresnegro = ('darkgray','darkgrey','darkslategray','grey','gray','dimgray','dimgrey','lightslategray','slategray','slategrey','darkslategrey','black')


#variable que contendra los valores que se ingresaran a la red neuronal
entrada = [0, 0, 0, 0, 0, 0, 0, 0]

#METODOS

#recibe el rgb para poder hacer un aproximado del color que se esta evaluando
def closest_colour(requested_colour):
    min_colours = {}
    for key, name in webcolors.css3_hex_to_names.items():
        r_c, g_c, b_c = webcolors.hex_to_rgb(key)
        rd = (r_c - requested_colour[0]) ** 2
        gd = (g_c - requested_colour[1]) ** 2
        bd = (b_c - requested_colour[2]) ** 2
        min_colours[(rd + gd + bd)] = name
    return min_colours[min(min_colours.keys())]

#reciste metodo recibe el aproximado del color y devuelve el nombre del color
def get_colour_name(requested_colour):
    try:
        closest_name = actual_name = webcolors.rgb_to_name(requested_colour)
    except ValueError:
        closest_name = closest_colour(requested_colour)
        actual_name = None
        #print "Nombre actual del color:", actual_name, ",Nombre del color mas cercano:", closest_name
        #if (closest_name == 'sienna'):
            #print (requested_colour)
    return actual_name, closest_name

#se busca formas circulares por medio del contorno para encontrar la fruta
def encontrarcirculos(nameimage):

    #lo primero que se hace es redimensionara la imagen original, si la imagen es muy grande es dificil
    #que encuentre formas circular y el contorno
    path = './'
    imagpath = './imagenes/'+nameimage+'.jpg'
    datos = cv2.imread(imagpath)
    original = Image.open (imagpath,mode='r')
    h, w = datos.shape[:2]

    if (h>w):
        original = original.resize((550,750), Image.ANTIALIAS)
    else:
        original = original.resize((750,550), Image.ANTIALIAS)

    original.save(path+'redim.jpg')


    #se aplican filtros para poder encontrar el contorno
    raw_image = cv2.imread(path+'redim.jpg')
    bilateral_filtered_image = cv2.bilateralFilter(raw_image, 5, 175, 175)
    edge_detected_image = cv2.Canny(bilateral_filtered_image, 75, 200)

    _, contours, hierarchy = cv2.findContours(edge_detected_image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    #teniendo el contorno se buscan formas circulares dentro del contorno
    #los contornos que no tienen forma circular se eliminan
    contour_list = []
    listap = np.array([])
    for contour in contours:
        approx = cv2.approxPolyDP(contour,0.01*cv2.arcLength(contour,True),True)
        area = cv2.contourArea(contour)
        if ((len(approx) > 8) & (len(approx) < 23) & (area > 30) ):
            contour_list.append(contour)
            listap=np.append(listap,contour)


    #teniendo los contornos donde hay circulos se pasan todo  todo a un vector
    contour_list2 = np.vstack(contour_list).squeeze()

    var=np.array(contour_list)

    #se busca la x,y minima, x,y maxima para y se obtiene dos puntos para encerrar la imagen
    xmax =contour_list2[:,0].max()
    xmin =contour_list2[:,0].min()
    ymax =contour_list2[:,1].max()
    ymin =contour_list2[:,1].min()


    mediox = (xmax + xmin)//2
    medioy = (ymax + ymin)//2

    iniverdex = mediox-200
    iniverdey = medioy-200
    finverdex = mediox+200
    finverdey = medioy+200


    while iniverdex<xmin or iniverdey<ymin:
    	iniverdex = iniverdex+50
    	iniverdey = iniverdey+50
    	finverdex = finverdex-50
    	finverdey = finverdey-50

    #se recorta la imagen donde esta la fruta, para no utilizar toda la imagen
    imCrop = raw_image[iniverdey:finverdey,iniverdex:finverdex]
    cv2.imshow("Recorte", imCrop)
    cv2.imwrite('recorte.jpg',imCrop)


    cv2.drawContours(raw_image, contour_list,  -1, (255,0,0), 1)

    #cuadro original
    cv2.rectangle(raw_image, (xmin,ymin) ,(xmax,ymax),(0,0,255),1)
    cv2.rectangle(raw_image,(iniverdex,iniverdey),(finverdex,finverdey),(0,255,0),1)#verde
    cv2.imshow('Cuadro medio',raw_image)

    cv2.waitKey(0)


#nameimg es el nombre de la imagen a redimensionar, y newimg es el nombre que se desea que tenga dicha imagen ya redimensionada
def redimensionarimagen(nameimg,newimg):
    #nameimg =''
    path= './'

    imagpath = path+nameimg+'.jpg'
    imag = Image.open (imagpath,mode='r')
    imag = imag.resize((350, 350), Image.ANTIALIAS)
    #imag = imag.convert ("L")
    imag.save("./"+newimg+'.jpg')

#nameimg es el nombre de la imagen a la cual se obtendra la informacion de cada pixel y se normalizara
def normalizarentradas(nameimg):
    path= './'

    nameimg = path+nameimg+'.jpg'
    lista = []
    listaC = []
    global entrada
    im2 = im.open(nameimg)
    im3 = im2.convert("RGB")

    pixels = list(im3.getdata())

    #cada contador almacena la cantidad de pixeles que pertenecen a una gama de colores
    CONTcoloresrojos =  0
    CONTcoloresnaranjas = 0
    CONTcoloresverdeoscuro = 0
    CONTcoloresverdeclaro = 0
    CONTcoloresamarillosclaro = 0
    CONTcoloresamarillososcuro = 0
    CONTcolorescafe = 0
    CONTcoloresnegro = 0


    for dato in range(122500):
        actual_name, closest_name = get_colour_name(pixels[dato])

        if (closest_name not in lista):
             lista.append(closest_name)

        #en cada for se tiene el nombre del pixel y se busca en cada gama de colores para aumentar el contador donde esta el color
        for iterador in range(len(coloresrojos)):
            if closest_name == coloresrojos[iterador]:
                 CONTcoloresrojos+= 1
        for iterador in range(len(coloresnaranjas)):
            if closest_name == coloresnaranjas[iterador]:
                CONTcoloresnaranjas += 1
        for iterador in range(len(coloresverdeoscuro)):
            if closest_name == coloresverdeoscuro[iterador]:
                CONTcoloresverdeoscuro += 1
        for iterador in range(len(coloresverdeclaro)):
            if closest_name == coloresverdeclaro[iterador]:
                CONTcoloresverdeclaro += 1
        for iterador in range(len(coloresamarillosclaro)):
            if closest_name == coloresamarillosclaro[iterador]:
                CONTcoloresamarillosclaro += 1
        for iterador in range(len(coloresamarillososcuro)):
            if closest_name == coloresamarillososcuro[iterador]:
                CONTcoloresamarillososcuro += 1
        for iterador in range(len(colorescafe)):
            if closest_name == colorescafe[iterador]:
                CONTcolorescafe += 1
        for iterador in range(len(coloresnegro)):
            if closest_name == coloresnegro[iterador]:
                CONTcoloresnegro += 1

    entrada = [CONTcoloresrojos, CONTcoloresnaranjas, CONTcoloresverdeoscuro, CONTcoloresverdeclaro, CONTcoloresamarillosclaro, CONTcoloresamarillososcuro,CONTcolorescafe,CONTcoloresnegro]

    #print ("Contadores",entrada)
    print ("\nTotal pixeles",np.sum(entrada))


    #al tener todos los contadores se hace un porcentaje y se redondea con dos decimales
    CONTcoloresrojos = (CONTcoloresrojos *100) / 122500
    CONTcoloresnaranjas = (CONTcoloresnaranjas * 100) / 122500
    CONTcoloresverdeoscuro = (CONTcoloresverdeoscuro * 100) / 122500
    CONTcoloresverdeclaro = (CONTcoloresverdeclaro * 100) / 122500
    CONTcoloresamarillosclaro = (CONTcoloresamarillosclaro * 100) / 122500
    CONTcoloresamarillososcuro = (CONTcoloresamarillososcuro * 100) / 122500
    CONTcolorescafe = (CONTcolorescafe * 100) / 122500
    CONTcoloresnegro = (CONTcoloresnegro * 100) / 122500

    CONTcoloresrojos =  round(CONTcoloresrojos, 2)
    CONTcoloresnaranjas = round(CONTcoloresnaranjas, 2)
    CONTcoloresverdeoscuro = round(CONTcoloresverdeoscuro, 2)
    CONTcoloresverdeclaro = round(CONTcoloresverdeclaro, 2)
    CONTcoloresamarillosclaro = round(CONTcoloresamarillosclaro, 2)
    CONTcoloresamarillososcuro = round(CONTcoloresamarillososcuro, 2)
    CONTcolorescafe = round(CONTcolorescafe, 2)
    CONTcoloresnegro = round(CONTcoloresnegro, 2)

    #print (lista)
    entrada = [CONTcoloresrojos, CONTcoloresnaranjas, CONTcoloresverdeoscuro, CONTcoloresverdeclaro, CONTcoloresamarillosclaro, CONTcoloresamarillososcuro,CONTcolorescafe,CONTcoloresnegro]
    #print ("Entrada: ",entrada)
    print ("\nRojo",entrada[0]),"\n"
    print ("Naranja",entrada[1]),"\n"
    print ("Verde O",entrada[2]),"\n"
    print ("Verde C",entrada[3]),"\n"
    print ("Amarillo C",entrada[4]),"\n"
    print ("Amarillo O",entrada[5]),"\n"
    print ("Cafe",entrada[6]),"\n"
    print ("Negro",entrada[7]),"\n"

    print ("Suma",np.sum(entrada))

def red():
    global entrada
    entrada = [entrada]
    #se carga la red neuronal ya entrenada para poder usarla
    net = nl.load('test.net')
    #se envia la entrada a la red para conocer la salida
    out = net.sim(entrada)
    print ("\n\nSalida\n",out)

    s1 = out[0][0]
    s2 = out[0][1]
    s3 = out[0][2]




    #teniendo la salida se verifica que topo de salida es y se da la descripcion de la salida
    #teniendo la salida ya se determina como esta la fruta
    #1,0,0
    if((s1>=0.96) and (s2<0.96) and (s3<0.96)):
        #print (s1,s2,s3)
        print ("Nivel de madurez: Verde, no comestible")
        print ("Dias estimados: 8 dias")
    #1,1,0
    elif((s1>=0.96) and (s2>=0.96) and (s3<0.96)):
        #print (s1,s2,s3)
        print ("Nivel de madurez: Semiverde, poco comestible")
        print ("Dias estimados: 6 dias")
    #0,1,0
    elif((s1<0.96) and (s2>=0.96) and (s3<0.96)):
        #print (s1,s2,s3)
        print ("Nivel de madurez: Maduro, comestible")
        print ("Dias estimados: 4 dias")
    #0,1,1
    elif((s1<0.96) and (s2>=0.96) and (s3>=0.96)):
        #print (s1,s2,s3)
        print ("Nivel de madurez: Maduro antes de podrirse,poco comestible")
        print ("Dias estimados: 2 dias")
    #1,1,1
    elif((s1>=0.96) and (s2>=0.96) and (s3>=0.96)):
        #print (s1,s2,s3)
        print ("Nivel de madurez: Podrido, no comestible")
        print ("Dias estimados: 0 dias")
    #0,0,0
    elif((s1<0.96) and (s2<0.96) and (s3<0.96)):
        #print (s1,s2,s3)
        print ("Error")
    #0,0,1
    elif((s1<0.96) and (s2<0.96) and (s3>=0.96)):
        #print (s1,s2,s3)
        print ("Error")
    #1,0,1
    elif((s1>=0.99) and (s2<0.99) and (s3>=0.99)):
        #print (s1,s2,s3)
        print ("Error")


encontrarcirculos(nameimage)
redimensionarimagen('recorte',('recorte-350x350'))
print ("Nombre imagen:",(nameimage+".jpg"))
normalizarentradas(('recorte-350x350'))
red()
